module gitlab.com/jgero/my-webpage

go 1.16

require (
	github.com/gomarkdown/markdown v0.0.0-20210820032736-385812cbea76
	github.com/minio/minio-go/v7 v7.0.12
	golang.org/x/net v0.0.0-20210813160813-60bc85c4be6d
)
