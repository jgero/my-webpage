package main

import (
	"fmt"
	"os"

	"gitlab.com/jgero/my-webpage/internal/locales"
	"gitlab.com/jgero/my-webpage/internal/store"
	"gitlab.com/jgero/my-webpage/internal/webserver"
)

func main() {
	layoutGen := &locales.StaticLayoutLoader{}
	conn, err := store.GetObjectstoreConnection()
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}

	server := webserver.UiWebserver{
		Port:                   ":" + os.Getenv("SERVER_PORT"),
		ImageResourceStoreConn: conn,
		LandingpageGen:         &locales.StaticLandingpageLoader{LayoutGen: layoutGen},
		ErrorpageGen:           &locales.StaticErrorpageLoader{LayoutGen: layoutGen},
		BlogpageGen:            &locales.StorageBlogpageLoader{LayoutGen: layoutGen, Conn: conn},
		OverviewpageGen:        &locales.StorageOverviewLoader{LayoutGen: layoutGen, Conn: conn},
	}
	server.StartServer()
}
