# jgero.me webpage

This project is a server side rendered webpage using golang templates. The pages are partly rendered
from static locales and partly from markdown files loaded from a object store bucket. It is intended
to run in containers and I use podman and podman-compose for local development.

## develop

With podman-compose a local development environment including an object store can be pulled up.
There is a compressed (via `tar -cvpzf source target`) version in the `build/ci` directory of the
data directory of a minio instance. That means by extracting and mounting that archive onto the
minio container the complete access key and data setup is already done.

