fmt_check:
	podman build \
		-f "build/ci/formatting.Containerfile" \
		-t "my-webpage-formatting:dev" \
		$(PWD)
	podman run \
		--rm \
		my-webpage-formatting:dev

unittests:
	podman build \
		-f "build/ci/unittests.Containerfile" \
		-t "my-webpage-unittests:dev" \
		$(PWD)
	podman run \
		--rm \
		my-webpage-unittests:dev

fmt_apply:
	podman build \
		-f "build/ci/formatting.Containerfile" \
		-t "my-webpage-formatting:dev" \
		$(PWD)
	podman run \
		--rm \
		-v "$(PWD):/app/code:z" \
		-e MODE=apply \
		my-webpage-formatting:dev

build:
	bash scripts/build_prod.sh

start: build
	podman play kube build/package/my-webpage-prod.yml

dev: stop
	podman build \
		-t my-webpage/go:dev \
		-f ${PWD}/build/package/go-dev.Containerfile \
		.
	podman play kube ${PWD}/build/package/my-webpage-dev-pod.yaml

rebuild_webserver:
	podman build \
		-t my-webpage/go:dev \
		-f ${PWD}/build/package/go-dev.Containerfile \
		.
	podman container stop my-webpage-dev-webserver
	podman container rm my-webpage-dev-webserver
	podman container run \
		-d \
		--pod my-webpage-dev \
		--name my-webpage-dev-webserver \
		my-webpage/go:dev
 
stop:
	bash scripts/stop.sh

clean:
	bash scripts/clean.sh

.PHONY: fmt_check fmt_apply build start dev stop clean rebuild_webserver
