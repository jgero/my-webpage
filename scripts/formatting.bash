#!/usr/bin/env bash

EXIT_CODE=0

if [ "$MODE" = "check" ]; then

	# check go formatting
	echo "==> checking with gofmt..."
	GOFMT_FILES=$(gofmt -l `find . -name '*.go' | grep -v vendor`)
	if [[ -n ${GOFMT_FILES} ]]; then
		echo "==> following files need fixing:"
		echo "${GOFMT_FILES}"
		echo "==> [ERROR]"
		echo
		EXIT_CODE=1
	else
		echo "==> [DONE]"
	fi

fi

if [ "$MODE" = "apply" ]; then

	# apply formatting
	echo "==> apply with gofmt..."
	gofmt -w .

fi

exit $EXIT_CODE
