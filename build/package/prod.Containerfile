# build the go server
FROM docker.io/golang:1.16.4 AS go-builder

WORKDIR /app

# install dependencies
COPY go.mod go.mod
COPY go.sum go.sum
RUN go mod download

# copy sources and compile
COPY cmd cmd
COPY internal internal
RUN CGO_ENABLED=0  GOOS=linux GOARCH=amd64 go build -ldflags="-w -s" -o /out/webserver cmd/webserver/webserver.go

# use minimal image as final stage
FROM scratch

# set 8080 as port and expose
ENV SERVER_PORT 8080
EXPOSE 8080

# app expects object store port to be default in prod
# ENV OBJECT_STORE_PORT 9000
# set url to strato object store in prod
ENV OBJECT_STORE_URL s3.hidrive.strato.com

# copy certificates to all standard authorities from builder
COPY --from=go-builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/

# copy binary and run it
COPY --from=go-builder /out/webserver /app/webserver
ENTRYPOINT ["/app/webserver"]
