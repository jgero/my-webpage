package locales_test

import (
	"context"
	"strings"
	"testing"

	"gitlab.com/jgero/my-webpage/internal/constants"
	"gitlab.com/jgero/my-webpage/internal/locales"
	"gitlab.com/jgero/my-webpage/internal/store"
)

func TestBlogpageLocalesGerator(t *testing.T) {
	spy := store.DefaultConnectionSpy{
		UpstreamData:             "this is my upstream data string",
		UpstreamType:             store.TEXT,
		GetObjectCalledCounter:   0,
		WriteObjectCalledCounter: 0,
	}
	gen := locales.StorageBlogpageLoader{
		Conn:      &spy,
		LayoutGen: &locales.StaticLayoutLoader{},
	}
	ctx := context.WithValue(context.Background(), constants.LanguageCodeContextKey, locales.EN)
	ctx = context.WithValue(ctx, constants.BlogpostKey, "my-post")
	locales, err := gen.Generate(ctx)
	if err != nil {
		t.Logf("generating locales should work without error: %s", err.Error())
		t.FailNow()
	}
	if !strings.Contains(locales.HtmlBlogpost, spy.UpstreamData) {
		t.Log("generated html content of the blog does not contain the upstream data")
		t.Fail()
	}
	if spy.GetObjectCalledCounter != 1 {
		t.Log("generator should load the object from the store")
		t.Fail()
	}
	if spy.WriteObjectCalledCounter != 0 {
		t.Log("generator should not write anything to the store")
		t.Fail()
	}
}
