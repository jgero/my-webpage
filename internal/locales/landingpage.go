package locales

import (
	"context"
	"fmt"
)

type LandingpageLocales struct {
	*LayoutLocales
	Title    string
	Header   string
	Sections *[]LandingpageSection
}

type LandingpageSection struct {
	Title  string
	Topics *[]LandingpageTopic
}

type LandingpageTopic struct {
	MaterialIcon string
	Description  string
	Link         string
}

type StaticLandingpageLoader struct {
	LayoutGen LayoutLocalesGenerator
}

type LandingpageLocalesGenerator interface {
	Generate(context.Context) (*LandingpageLocales, error)
}

func (gen *StaticLandingpageLoader) Generate(ctx context.Context) (*LandingpageLocales, error) {
	lang, err := GetLanguageFromContext(ctx)
	if err != nil {
		return nil, &LocalizationError{Message: "could not load language", Err: err}
	}
	var layoutLocales *LayoutLocales
	layoutLocales, err = gen.LayoutGen.Generate(ctx)
	if err != nil {
		return nil, err
	}
	switch lang {
	case EN:
		return &LandingpageLocales{
			LayoutLocales: layoutLocales,
			Title:         "Hello - Johannes Gerold",
			Header:        "hello there!",
			Sections: &[]LandingpageSection{
				{
					Title: "who am i?",
					Topics: &[]LandingpageTopic{
						{
							MaterialIcon: "account_circle",
							Description:  "Read more about me",
							Link:         LocalizeUrl("/about_me", layoutLocales.LanguageCode),
						},
					},
				},
				{
					Title: "what am i doing?",
					Topics: &[]LandingpageTopic{
						{
							MaterialIcon: "build",
							Description:  "See what I am building",
							Link:         LocalizeUrl("/blog/projects", layoutLocales.LanguageCode),
						},
						{
							MaterialIcon: "article",
							Description:  "I write a blog about things I find interesting and helpful",
							Link:         LocalizeUrl("/blog", layoutLocales.LanguageCode),
						},
					},
				},
			},
		}, nil
	case DE:
		return &LandingpageLocales{
			LayoutLocales: layoutLocales,
			Title:         "Hallo - Johannes Gerold",
			Header:        "Hey!",
			Sections: &[]LandingpageSection{
				{
					Title: "Wer bin ich?",
					Topics: &[]LandingpageTopic{
						{
							MaterialIcon: "account_circle",
							Description:  "Lese hier mehr über mich",
							Link:         LocalizeUrl("/about_me", layoutLocales.LanguageCode),
						},
					},
				},
				{
					Title: "Was mache ich?",
					Topics: &[]LandingpageTopic{
						{
							MaterialIcon: "build",
							Description:  "Schau dir an was ich baue",
							Link:         LocalizeUrl("/blog/projects", layoutLocales.LanguageCode),
						},
						{
							MaterialIcon: "article",
							Description:  "Ich schreibe einen Blog über sachen die ich interessant oder hilfreich finde",
							Link:         LocalizeUrl("/blog", layoutLocales.LanguageCode),
						},
					},
				},
			},
		}, nil
	default:
		return nil, &LocalizationError{
			Message: fmt.Sprintf("'%s' is an invalid langauge", lang.String()),
		}
	}
}

type MockLandingpageLoader struct {
	LayoutGen LayoutLocalesGenerator
	Title     string
	Header    string
	Sections  *[]LandingpageSection
}

func (gen *MockLandingpageLoader) Generate(ctx context.Context) (*LandingpageLocales, error) {
	layoutLocales, err := gen.LayoutGen.Generate(ctx)
	if err != nil {
		return nil, err
	}
	return &LandingpageLocales{
		LayoutLocales: layoutLocales,
		Title:         gen.Title,
		Header:        gen.Header,
		Sections:      gen.Sections,
	}, nil
}
