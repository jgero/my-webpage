package locales

import (
	"context"
	"fmt"
)

const ErrorCodeKey = "errorcode-key"

type ErrorpageLocales struct {
	*LayoutLocales
	Title            string
	ErrorCode        int
	ErrorExplanation string
	Action           string
}

type StaticErrorpageLoader struct {
	LayoutGen LayoutLocalesGenerator
}

type ErrorpageLocalesGenerator interface {
	Generate(context.Context) (*ErrorpageLocales, error)
}

var errorCodeLocales = map[Language]map[int]map[string]string{
	EN: {
		404: {
			"explanation": "page could not be found",
			"action":      "the link leading here may be broken or old",
		},
		500: {
			"explanation": "internal server error",
			"action":      "something went wrong on the server side while trying to serve your request",
		},
	},
	DE: {
		404: {
			"explanation": "seite konnte nicht gefunden werden",
			"action":      "Der Link der auf diese Seite geführt hat scheint kaputt zu sein",
		},
		500: {
			"explanation": "interner server-fehler",
			"action":      "Etwas ist beim Bearbeiten deiner Anfrage auf Serverseite fehlgeschlagen",
		},
	},
}

func (gen *StaticErrorpageLoader) Generate(ctx context.Context) (*ErrorpageLocales, error) {
	lang, err := GetLanguageFromContext(ctx)
	if err != nil {
		return nil, &LocalizationError{Message: "could not load language", Err: err}
	}
	errorCode, ok := ctx.Value(ErrorCodeKey).(int)
	if !ok {
		return nil, &LocalizationError{Message: "error code in context is not of type 'int'"}
	}
	var layoutLocales *LayoutLocales
	layoutLocales, err = gen.LayoutGen.Generate(ctx)
	if err != nil {
		return nil, err
	}
	var title string
	switch lang {
	case EN:
		title = "Oops!"
		break
	case DE:
		title = "Ups!"
		break
	default:
		return nil, &LocalizationError{
			Message: fmt.Sprintf("'%s' is an invalid langauge", lang.String()),
		}
	}
	return &ErrorpageLocales{
		LayoutLocales:    layoutLocales,
		Title:            title,
		ErrorCode:        errorCode,
		ErrorExplanation: errorCodeLocales[lang][errorCode]["explanation"],
		Action:           errorCodeLocales[lang][errorCode]["action"],
	}, nil
}

type MockErrorpageLoader struct {
	LayoutGen        LayoutLocalesGenerator
	Title            string
	ErrorCode        int
	ErrorExplanation string
	Action           string
}

func (gen *MockErrorpageLoader) Generate(ctx context.Context) (*ErrorpageLocales, error) {
	layoutLocales, err := gen.LayoutGen.Generate(ctx)
	if err != nil {
		return nil, err
	}
	return &ErrorpageLocales{
		LayoutLocales:    layoutLocales,
		Title:            gen.Title,
		ErrorCode:        gen.ErrorCode,
		ErrorExplanation: gen.ErrorExplanation,
		Action:           gen.Action,
	}, nil
}
