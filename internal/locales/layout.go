package locales

import (
	"context"
	"fmt"
)

type LayoutLocales struct {
	LanguageCode       Language
	ShowAction         string
	ReadAction         string
	SearchPlaceholder  string
	TocSubheader       string
	FootnotesSubheader string
	TopNavbar          *[]NavbarLink
	Footer             *[]NavbarLink
}

type NavbarLink struct {
	Title string
	Link  string
}

type StaticLayoutLoader struct{}

type LayoutLocalesGenerator interface {
	Generate(context.Context) (*LayoutLocales, error)
}

func (gen *StaticLayoutLoader) Generate(ctx context.Context) (*LayoutLocales, error) {
	lang, err := GetLanguageFromContext(ctx)
	if err != nil {
		return nil, &LocalizationError{Message: "could not load language", Err: err}
	}
	switch lang {
	case EN:
		return &LayoutLocales{
			LanguageCode:       lang,
			ShowAction:         "show me",
			ReadAction:         "read",
			SearchPlaceholder:  "search term",
			TocSubheader:       "Table of Contents",
			FootnotesSubheader: "Footnotes",
			TopNavbar: &[]NavbarLink{
				{
					Title: "johannes gerold",
					Link:  LocalizeUrl("/", lang),
				},
				{
					Title: "blog",
					Link:  LocalizeUrl("/blog", lang),
				},
				{
					Title: "portfolio",
					Link:  LocalizeUrl("/portfolio", lang),
				},
				{
					Title: "contact me",
					Link:  LocalizeUrl("/contact_me", lang),
				},
			},
			Footer: &[]NavbarLink{
				{
					Title: "DE",
					Link:  "/de/",
				},
				{
					Title: "about",
					Link:  LocalizeUrl("/about", lang),
				},
			},
		}, nil
	case DE:
		return &LayoutLocales{
			LanguageCode:       lang,
			ShowAction:         "anzeigen",
			ReadAction:         "lesen",
			SearchPlaceholder:  "Suchbegriff",
			TocSubheader:       "Inhaltstabelle",
			FootnotesSubheader: "Fußnoten",
			TopNavbar: &[]NavbarLink{
				{
					Title: "johannes gerold",
					Link:  LocalizeUrl("/", lang),
				},
				{
					Title: "blog",
					Link:  LocalizeUrl("/blog", lang),
				},
				{
					Title: "portfolio",
					Link:  LocalizeUrl("/portfolio", lang),
				},
				{
					Title: "kontakt",
					Link:  LocalizeUrl("/contact_me", lang),
				},
			},
			Footer: &[]NavbarLink{
				{
					Title: "EN",
					Link:  "/en/",
				},
				{
					Title: "Impressum",
					Link:  LocalizeUrl("/about", lang),
				},
			},
		}, nil
	default:
		return nil, &LocalizationError{
			Message: fmt.Sprintf("'%s' is an invalid langauge", lang.String()),
		}
	}
}
