package locales

import (
	"context"
	"fmt"
	"net/url"
	"sort"
	"strings"
	"time"

	"gitlab.com/jgero/my-webpage/internal/constants"
	"gitlab.com/jgero/my-webpage/internal/store"
)

type BlogpostListItem struct {
	Title           string
	DateISO         string
	DateDescription string
	Link            string
}

type OverviewLocales struct {
	*LayoutLocales
	Title        string
	Header       string
	Blogposts    *[]BlogpostListItem
	SearchString string
}

type StorageOverviewLoader struct {
	Conn      store.ObjectGetterWriter
	LayoutGen LayoutLocalesGenerator
}

type OverviewLocalesGenerator interface {
	Generate(context.Context) (*OverviewLocales, error)
}

func (gen *StorageOverviewLoader) Generate(ctx context.Context) (*OverviewLocales, error) {
	lang, err := GetLanguageFromContext(ctx)
	// check for ok is not needed, default will be "" which is fine
	searchString, _ := ctx.Value(constants.SearchStringKey).(string)
	if err != nil {
		return nil, err
	}
	var layoutLocales *LayoutLocales
	layoutLocales, err = gen.LayoutGen.Generate(ctx)
	if err != nil {
		return nil, err
	}
	var ovLoc *OverviewLocales
	switch lang {
	case EN:
		ovLoc = &OverviewLocales{
			LayoutLocales: layoutLocales,
			Title:         "Blog - Johannes Gerold",
			Header:        "Blogposts",
			Blogposts:     nil,
		}
		break
	case DE:
		ovLoc = &OverviewLocales{
			LayoutLocales: layoutLocales,
			Title:         "Blog - Johannes Gerold",
			Header:        "Blogposts",
			Blogposts:     nil,
		}
		break
	default:
		return nil, &LocalizationError{
			Message: fmt.Sprintf("'%s' is an invalid langauge", lang.String()),
		}
	}

	ovLoc.SearchString = searchString

	bucket := &store.Bucket{Name: "blogposts", Prefix: lang.String()}
	objectsInfo := store.BlobsInfo{ResidingBucket: bucket}
	err = gen.Conn.GetAllObjectInfos(ctx, &objectsInfo)
	if err != nil {
		return nil, err
	}
	sort.Slice(objectsInfo.Info, func(i, j int) bool {
		return objectsInfo.Info[i].LastModified.After(objectsInfo.Info[j].LastModified)
	})
	ovLoc.Blogposts = &[]BlogpostListItem{}
	for _, i := range objectsInfo.Info {
		title := store.KeyToTitle(i.Key)
		if !strings.Contains(title, searchString) {
			// skip this post if the key is not in the title
			continue
		}
		var prevPosts []BlogpostListItem
		if ovLoc.Blogposts == nil {
			prevPosts = make([]BlogpostListItem, 0)
		} else {
			prevPosts = *ovLoc.Blogposts
		}
		var temp []BlogpostListItem
		if lang == EN {
			temp = append(prevPosts, BlogpostListItem{
				Title:           title,
				DateISO:         i.LastModified.Format(time.RFC3339),
				DateDescription: fmt.Sprintf("%d/%d/%d", i.LastModified.Month(), i.LastModified.Day(), i.LastModified.Year()),
				Link:            LocalizeUrl(fmt.Sprintf("/blog/%s", url.PathEscape(store.KeyToTitle(i.Key))), lang),
			})
		} else {
			temp = append(prevPosts, BlogpostListItem{
				Title:           title,
				DateISO:         i.LastModified.Format(time.RFC3339),
				DateDescription: fmt.Sprintf("%d.%d.%d", i.LastModified.Day(), i.LastModified.Month(), i.LastModified.Year()),
				Link:            LocalizeUrl(fmt.Sprintf("/blog/%s", url.PathEscape(store.KeyToTitle(i.Key))), lang),
			})
		}
		ovLoc.Blogposts = &temp
	}
	return ovLoc, nil
}

type MockOverviewLoader struct {
	LayoutGen LayoutLocalesGenerator
	Title     string
	Header    string
	Paragraph string
}

func (gen *MockOverviewLoader) Generate(ctx context.Context) (*OverviewLocales, error) {
	layoutLocales, err := gen.LayoutGen.Generate(ctx)
	if err != nil {
		return nil, err
	}
	return &OverviewLocales{
		LayoutLocales: layoutLocales,
		Title:         gen.Title,
		Blogposts: &[]BlogpostListItem{
			{
				Title:           "this is a test",
				DateISO:         "2011-11-18T14:54:39.929Z",
				DateDescription: "11/18/2011",
				Link:            LocalizeUrl("/blog/test", layoutLocales.LanguageCode),
			},
		},
	}, nil
}
