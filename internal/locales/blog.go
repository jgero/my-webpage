package locales

import (
	"context"
	"fmt"

	"gitlab.com/jgero/my-webpage/internal/constants"
	"gitlab.com/jgero/my-webpage/internal/markdown"
	"gitlab.com/jgero/my-webpage/internal/store"
)

type BlogpageLocales struct {
	*LayoutLocales
	Title        string
	Description  string
	HtmlBlogpost string
}

type StorageBlogpageLoader struct {
	Conn      store.ObjectGetterWriter
	LayoutGen LayoutLocalesGenerator
}

type BlogpageLocalesGenerator interface {
	Generate(context.Context) (*BlogpageLocales, error)
}

func (gen *StorageBlogpageLoader) Generate(ctx context.Context) (*BlogpageLocales, error) {
	lang, err := GetLanguageFromContext(ctx)
	if err != nil {
		return nil, err
	}
	var layoutLocales *LayoutLocales
	layoutLocales, err = gen.LayoutGen.Generate(ctx)
	if err != nil {
		return nil, err
	}
	var bpLoc *BlogpageLocales
	switch lang {
	case EN:
		bpLoc = &BlogpageLocales{
			LayoutLocales: layoutLocales,
			Title:         "Blog - Johannes Gerold",
			HtmlBlogpost:  "",
		}
	case DE:
		bpLoc = &BlogpageLocales{
			LayoutLocales: layoutLocales,
			Title:         "Blog - Johannes Gerold",
			HtmlBlogpost:  "",
		}
	default:
		return nil, &LocalizationError{
			Message: fmt.Sprintf("'%s' is an invalid langauge", lang.String()),
		}
	}
	id, ok := ctx.Value(constants.BlogpostKey).(string)
	if !ok {
		return nil, &LocalizationError{Message: "blogpost id from context is not of type string (maybe does not even exist)"}
	}
	if id == "" {
		return nil, &LocalizationError{Message: "cannot generate locales for blogpost without Id"}
	}

	bucket := &store.Bucket{Name: "blogposts", Prefix: lang.String()}
	objectBlob := store.DataBlob{ResidingBucket: bucket, Key: store.TitleToKey(id)}
	err = gen.Conn.GetObject(ctx, &objectBlob)
	if err != nil {
		return nil, err
	}
	bpLoc.Title = fmt.Sprintf("%s - Blog - Johannes Gerold", id)
	bpLoc.HtmlBlogpost, bpLoc.Description = markdown.MarkdownToHtml(string(objectBlob.Data), store.TitleToPrefix(id))
	return bpLoc, nil
}

type MockBlogpageLoader struct {
	LayoutGen LayoutLocalesGenerator
	Title     string
	Header    string
	Paragraph string
}

func (gen *MockBlogpageLoader) Generate(ctx context.Context) (*BlogpageLocales, error) {
	layoutLocales, err := gen.LayoutGen.Generate(ctx)
	if err != nil {
		return nil, err
	}
	return &BlogpageLocales{
		LayoutLocales: layoutLocales,
		Title:         gen.Title,
		HtmlBlogpost:  "<h1>" + gen.Header + "</h1><p>" + gen.Paragraph + "</p>",
	}, nil
}
