package locales

import (
	"context"
	"fmt"

	"gitlab.com/jgero/my-webpage/internal/constants"
)

type Language int

const (
	DE Language = iota
	EN
)

func (l Language) String() string {
	switch l {
	case DE:
		return "de"
	case EN:
		return "en"
	}
	return "unknown"
}

func LocalizeUrl(baseUrl string, lang Language) string {
	return "/" + lang.String() + baseUrl
}

func GetLanguageFromContext(ctx context.Context) (Language, error) {
	// assert context value to langauge type
	lang, ok := ctx.Value(constants.LanguageCodeContextKey).(Language)
	if !ok {
		return EN, &LocalizationError{Message: "language in context is not of type 'locales.Language'"}
	}
	return lang, nil
}

type LocalizationError struct {
	Message string
	Err     error
}

func (e *LocalizationError) Error() string {
	if e.Err != nil {
		return fmt.Sprintf("could not generate locales: %s: %s", e.Message, e.Err.Error())
	} else {
		return fmt.Sprintf("could not generate locales: %s", e.Message)
	}
}

func (e *LocalizationError) Unwrap() error {
	return e.Err
}
