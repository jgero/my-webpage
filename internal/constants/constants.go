package constants

const (
	LanguageCodeContextKey = "lang"
	BlogpostKey            = "blogpost-key"
	SearchStringKey        = "search-string-key"
)
