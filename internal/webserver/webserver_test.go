package webserver_test

import (
	"context"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"gitlab.com/jgero/my-webpage/internal/constants"
	"gitlab.com/jgero/my-webpage/internal/locales"
	"gitlab.com/jgero/my-webpage/internal/store"
	"gitlab.com/jgero/my-webpage/internal/webserver"
)

func TestUiEndpoints(t *testing.T) {
	tests := []struct {
		name  string
		route string
		want  struct {
			upstream   string
			content    string
			statusCode int
		}
	}{
		{
			name:  "valid landingpage",
			route: "/en/",
			want: struct {
				upstream   string
				content    string
				statusCode int
			}{
				upstream:   "hello",
				content:    "hello",
				statusCode: http.StatusOK,
			},
		},
		{
			name:  "valid blog route",
			route: "/en/blog/test",
			want: struct {
				upstream   string
				content    string
				statusCode int
			}{
				upstream:   "#test\n\nthis is my nice test blogpost that should be rendered",
				content:    "this is my nice test blogpost that should be rendered",
				statusCode: http.StatusOK,
			},
		},
	}

	for _, tc := range tests {
		t.Run(tc.name, func(t *testing.T) {
			t.Parallel()

			request := httptest.NewRequest(http.MethodGet, tc.route, nil)
			request = request.WithContext(context.WithValue(request.Context(), constants.LanguageCodeContextKey, locales.EN))
			responseRecorder := httptest.NewRecorder()

			layoutGen := &locales.StaticLayoutLoader{}
			handler := &webserver.UiRouteHandler{
				LandingpageGen: &locales.StaticLandingpageLoader{LayoutGen: layoutGen},
				ErrorpageGen:   &locales.StaticErrorpageLoader{LayoutGen: layoutGen},
				BlogpageGen: &locales.StorageBlogpageLoader{LayoutGen: layoutGen, Conn: &store.DefaultConnectionSpy{
					UpstreamData:             tc.want.content,
					UpstreamType:             store.TEXT,
					GetObjectCalledCounter:   0,
					WriteObjectCalledCounter: 0,
				}},
			}
			handler.ServeHTTP(responseRecorder, request)

			if responseRecorder.Code != tc.want.statusCode {
				t.Errorf("wanted status code %d but got %d", tc.want.statusCode, responseRecorder.Code)
			}

			// only check content on requests that expect "ok" as response
			if tc.want.statusCode == http.StatusOK && !strings.Contains(responseRecorder.Body.String(), tc.want.content) {
				t.Errorf("wanted content was not in the response")
			}
		})
	}
}

func TestNonExistingBlogUiEndpoint(t *testing.T) {
	request := httptest.NewRequest(http.MethodGet, "/en/blog/test", nil)
	request = request.WithContext(context.WithValue(request.Context(), constants.LanguageCodeContextKey, locales.EN))
	responseRecorder := httptest.NewRecorder()

	layoutGen := &locales.StaticLayoutLoader{}
	handler := &webserver.UiRouteHandler{
		LandingpageGen: &locales.StaticLandingpageLoader{LayoutGen: layoutGen},
		ErrorpageGen:   &locales.StaticErrorpageLoader{LayoutGen: layoutGen},
		BlogpageGen: &locales.StorageBlogpageLoader{LayoutGen: layoutGen, Conn: &store.FailingConnectionSpy{
			UpstreamData:                   "",
			UpstreamType:                   store.TEXT,
			GetObjectCalledCounter:         0,
			WriteObjectCalledCounter:       0,
			GetAllObjectInfosCalledCounter: 0,
		}},
	}
	handler.ServeHTTP(responseRecorder, request)

	if responseRecorder.Code != http.StatusFound {
		t.Errorf("wanted status code %d but got %d", http.StatusFound, responseRecorder.Code)
	}

	if loc := responseRecorder.Result().Header.Get("Location"); loc != "/en/error/404" {
		t.Errorf("wanted redirect to 404 error page but got %s", loc)
	}
}
