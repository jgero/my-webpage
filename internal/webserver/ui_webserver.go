package webserver

import (
	"context"
	"embed"
	"fmt"
	"io/fs"
	"log"
	"net/http"
	"os"
	"os/signal"
	"regexp"
	"sync"
	"time"

	"gitlab.com/jgero/my-webpage/internal/constants"
	"gitlab.com/jgero/my-webpage/internal/locales"
	"gitlab.com/jgero/my-webpage/internal/store"
)

// TODO: maybe refactor the whole tracing and logging thing into its own package
// this would also enable deciding where to dump the logs at one common point
const (
	requestIDKey = "request-id"
)

//go:embed ui/static/*
var staticFiles embed.FS

// TODO: get some better ui algorithm and put it into the logging/tracing package
func getNextRequestId() string {
	return fmt.Sprintf("%d", time.Now().UnixNano())
}

// this struct is used to enable dependency injection for all the different loaders
type UiWebserver struct {
	Port string

	ImageResourceStoreConn store.ObjectGetterWriter

	LandingpageGen  locales.LandingpageLocalesGenerator
	ErrorpageGen    locales.ErrorpageLocalesGenerator
	BlogpageGen     locales.BlogpageLocalesGenerator
	OverviewpageGen locales.OverviewLocalesGenerator

	logger      *log.Logger
	isHealthy   bool
	healthMutex *sync.Mutex
}

func (serv *UiWebserver) StartServer() {
	// init health
	serv.isHealthy = false
	serv.healthMutex = &sync.Mutex{}

	serv.logger = log.New(os.Stdout, "http server: ", log.LstdFlags)
	serv.logger.Println("server is starting...")

	router := http.NewServeMux()

	// add health check endpoint
	router.Handle("/health", serv.healthz())

	// add handler for blog image resources
	router.Handle("/resources/images/", &ImageRouteHander{Conn: serv.ImageResourceStoreConn})

	// trim the "ui/static/"
	if trimmedStaticFs, err := fs.Sub(staticFiles, "ui/static"); err != nil {
		serv.logger.Printf("could not get subdir of static files: %s\n", err.Error())
	} else {
		// serve the trimmed static fs
		router.Handle("/static/", http.StripPrefix("/static/", http.FileServer(http.FS(trimmedStaticFs))))
	}

	uiHandler := UiRouteHandler{
		LandingpageGen:  serv.LandingpageGen,
		ErrorpageGen:    serv.ErrorpageGen,
		BlogpageGen:     serv.BlogpageGen,
		OverviewpageGen: serv.OverviewpageGen,
	}

	// langauge is put into the request context in localize middelware
	router.Handle("/en/", serv.localize()(&uiHandler))
	router.Handle("/de/", serv.localize()(&uiHandler))
	router.Handle("/", serv.localize()(&uiHandler))

	// apply middlewares, logger and timeouts
	server := &http.Server{
		Addr: serv.Port,
		// chain the middlewares and add router as last in the chain
		Handler:      tracing()(serv.logging()(router)),
		ErrorLog:     serv.logger,
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 10 * time.Second,
		IdleTimeout:  15 * time.Second,
	}

	done := make(chan bool)
	quit := make(chan os.Signal, 1)
	// push into quit channel on os interrupts
	signal.Notify(quit, os.Interrupt)

	go func() {
		// wait until quit channel has a message
		<-quit
		serv.logger.Println("server is shutting down...")

		// mark server as unhealthy
		serv.healthMutex.Lock()
		serv.isHealthy = false
		serv.healthMutex.Unlock()

		// give server 30 seconds to shutdown
		ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
		defer cancel()

		// because server is shutting down stop http keepalives
		server.SetKeepAlivesEnabled(false)
		if err := server.Shutdown(ctx); err != nil {
			serv.logger.Printf("could not shut server down gracefully: %s\n", err.Error())
		}
		// close done channel to trigger final shutdown code in parent function
		close(done)
	}()

	serv.logger.Printf("server is ready to handle requests at %s\n", serv.Port)
	serv.healthMutex.Lock()
	serv.isHealthy = true
	serv.healthMutex.Unlock()
	if err := server.ListenAndServe(); err != nil && err != http.ErrServerClosed {
		serv.logger.Printf("could not listen on %s: %s\n", serv.Port, err.Error())
		os.Exit(1)
	}

	// wait for done channel to log that server is stopped
	<-done
	serv.logger.Println("server stopped")

}

// set language context depending on route parameter or langauge header
func (serv *UiWebserver) localize() func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			deRegexp := regexp.MustCompile("^/de.*")
			// set german context
			if deRegexp.MatchString(r.URL.Path) {
				ctx := context.WithValue(r.Context(), constants.LanguageCodeContextKey, locales.DE)
				next.ServeHTTP(w, r.WithContext(ctx))
				return
			}
			enRegexp := regexp.MustCompile("^/en.*")
			// set english context
			if enRegexp.MatchString(r.URL.Path) {
				ctx := context.WithValue(r.Context(), constants.LanguageCodeContextKey, locales.EN)
				next.ServeHTTP(w, r.WithContext(ctx))
				return
			}
			lang := r.Header.Get("Accept-language")
			// if language header contains 'de' redirect to german homepage, otherwise use english
			if deRegexp.MatchString(lang) {
				http.Redirect(w, r, "/de/", http.StatusFound)
			} else {
				http.Redirect(w, r, "/en/", http.StatusFound)
			}
		})
	}

}

// tracing middleware
// sets http x-request-id header and context
func tracing() func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			requestId := r.Header.Get("X-Request-Id")
			if requestId == "" {
				requestId = getNextRequestId()
			}
			ctx := context.WithValue(r.Context(), requestIDKey, requestId)
			w.Header().Set("X-Request-Id", requestId)
			next.ServeHTTP(w, r.WithContext(ctx))
		})
	}

}

// logging middleware
// logs all requests to the provided logger
func (serv *UiWebserver) logging() func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			// log after the request was served
			defer func() {
				requestID, ok := r.Context().Value(requestIDKey).(string)
				if !ok {
					requestID = "unknown"
				}
				serv.logger.Println(requestID, r.Method, r.URL.Path)
			}()
			next.ServeHTTP(w, r)
		})
	}
}

// endpoint for health check
func (serv *UiWebserver) healthz() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		serv.healthMutex.Lock()
		healthStatusHealthy := serv.isHealthy
		serv.healthMutex.Unlock()
		if healthStatusHealthy {
			w.WriteHeader(http.StatusNoContent)
			return
		}
		w.WriteHeader(http.StatusServiceUnavailable)
	})
}
