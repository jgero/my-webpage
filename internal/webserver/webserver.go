package webserver

type Webserver interface {
	StartServer()
}
