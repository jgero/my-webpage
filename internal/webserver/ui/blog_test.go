package ui_test

import (
	"bytes"
	"context"
	"fmt"
	"testing"

	"gitlab.com/jgero/my-webpage/internal/constants"
	"gitlab.com/jgero/my-webpage/internal/locales"
	"gitlab.com/jgero/my-webpage/internal/webserver/ui"
)

func TestRenderBlogpage(t *testing.T) {
	tests := []struct {
		title     string
		header    string
		paragraph string
	}{
		{title: "my title should be in the meta title", header: "my header is in a h1", paragraph: "some text in a p element"},
		{title: "my other title should be there too", header: "my other header is in a h1 as well", paragraph: "and obv some text too"},
	}

	for index, tc := range tests {
		tc := tc
		t.Run(fmt.Sprintf("errorpage testcase %d", index), func(t *testing.T) {
			t.Parallel()

			ctx := context.WithValue(context.Background(), constants.LanguageCodeContextKey, locales.EN)

			uiEl := &ui.Blogpage{
				LocaleGenerator: &locales.MockBlogpageLoader{
					LayoutGen: &locales.StaticLayoutLoader{},
					Title:     tc.title,
					Header:    tc.header,
					Paragraph: tc.paragraph,
				},
			}
			var buf bytes.Buffer
			if err := uiEl.Render(ctx, &buf); err != nil {
				t.Log("should not return an error when rendering the blogpage with valid data")
				t.Logf("%s", err.Error())
				t.FailNow()
			}
			if &buf == nil {
				t.Log("rendering the template should not result in an 'nil' buffer")
				t.FailNow()
			}

			tokenList := []string{
				tc.title,
				tc.header,
				tc.paragraph,
			}
			hasToken, err := checkHtmlDocForTextNodes(&buf, tokenList)
			if err != nil {
				t.Log("rendered document should be parseable and not encounter error tokens")
				t.Logf("%s", err.Error())
				t.FailNow()
			}
			for i, v := range hasToken {
				if !v {
					t.Errorf("token '%s' was not found in the document", tokenList[i])
				}
			}
		})
	}
}
