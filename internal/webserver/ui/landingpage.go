package ui

import (
	"context"
	"io"

	"gitlab.com/jgero/my-webpage/internal/locales"
)

var (
	landingpage = parse("landingpage", []string{
		"templates/landingpage/index.html",
		"templates/landingpage/style.html",
	})
)

type Landingpage struct {
	locales         *locales.LandingpageLocales
	LocaleGenerator locales.LandingpageLocalesGenerator
}

func (l *Landingpage) Render(ctx context.Context, w io.Writer) error {
	if l.LocaleGenerator == nil {
		return &TemplateRenderingError{uiComponentName: "landingpage", Err: &locales.LocalizationError{Message: "no locale generator provided"}}
	}
	if l.locales == nil {
		var err error
		if l.locales, err = l.LocaleGenerator.Generate(ctx); err != nil {
			return err
		}
	}
	if buf, err := renderTemplateToBuffer(landingpage, l.locales); err != nil {
		return err
	} else {
		buf.WriteTo(w)
		return nil
	}
}
