package ui_test

import (
	"bytes"
	"context"
	"errors"
	"fmt"
	"testing"

	"gitlab.com/jgero/my-webpage/internal/constants"
	"gitlab.com/jgero/my-webpage/internal/locales"
	"gitlab.com/jgero/my-webpage/internal/webserver/ui"
)

func TestRenderLandingpage(t *testing.T) {
	tests := []struct {
		Title    string
		Header   string
		Sections *[]locales.LandingpageSection
		Lang     locales.Language
	}{
		{
			Lang:   locales.EN,
			Title:  "Hello - Johannes Gerold",
			Header: "hello there!",
			Sections: &[]locales.LandingpageSection{
				{
					Title: "who am i?",
					Topics: &[]locales.LandingpageTopic{
						{
							MaterialIcon: "account_circle",
							Description:  "Read more about me",
							Link:         "/about_me",
						},
					},
				},
				{
					Title: "what am i doing?",
					Topics: &[]locales.LandingpageTopic{
						{
							MaterialIcon: "build",
							Description:  "See what I am building",
							Link:         "/blog/projects",
						},
						{
							MaterialIcon: "article",
							Description:  "I write a blog about things I find interesting and helpful",
							Link:         "/blog",
						},
					},
				},
			},
		}, {
			Title:  "Hallo - Johannes Gerold",
			Header: "Hey!",
			Lang:   locales.DE,
			Sections: &[]locales.LandingpageSection{
				{
					Title: "Wer bin ich?",
					Topics: &[]locales.LandingpageTopic{
						{
							MaterialIcon: "account_circle",
							Description:  "Lese hier mehr über mich",
							Link:         "/about_me",
						},
					},
				},
				{
					Title: "Was mache ich?",
					Topics: &[]locales.LandingpageTopic{
						{
							MaterialIcon: "build",
							Description:  "Schau dir an was ich baue",
							Link:         "/blog/projects",
						},
						{
							MaterialIcon: "article",
							Description:  "Ich schreibe einen Blog über sachen die ich interessant oder hilfreich finde",
							Link:         "/blog",
						},
					},
				},
			},
		},
	}

	for index, tc := range tests {
		tc := tc
		var buf bytes.Buffer
		t.Run(fmt.Sprintf("landingpage testcase %d", index), func(t *testing.T) {
			t.Parallel()

			ctx := context.WithValue(context.Background(), constants.LanguageCodeContextKey, tc.Lang)

			uiEl := ui.Landingpage{LocaleGenerator: &locales.MockLandingpageLoader{
				LayoutGen: &locales.StaticLayoutLoader{},
				Title:     tc.Title,
				Header:    tc.Header,
				Sections:  tc.Sections,
			}}

			if err := uiEl.Render(ctx, &buf); err != nil {
				t.Logf("%s", errors.Unwrap(err))
				t.Logf("%s", err.Error())
				t.FailNow()
			}
			if &buf == nil {
				t.Log("rendering the template should not result in an 'nil' buffer")
				t.FailNow()
			}

			tokenList := []string{
				tc.Header, tc.Title,
			}
			for _, section := range *tc.Sections {
				tokenList = append(tokenList, section.Title)
				for _, topic := range *section.Topics {
					tokenList = append(tokenList, topic.Description)
				}

			}
			hasToken, err := checkHtmlDocForTextNodes(&buf, tokenList)
			if err != nil {
				t.Log("rendered document should be parseable and not encounter error tokens")
				t.Logf("%s", err.Error())
				t.FailNow()
			}
			for i, v := range hasToken {
				if !v {
					t.Errorf("token '%s' was not found in the document", tokenList[i])
				}
			}
		})
	}
}
