package ui

import (
	"context"
	"io"

	"gitlab.com/jgero/my-webpage/internal/locales"
)

var (
	errorPage = parse("errorpage", []string{
		"templates/error/index.html",
	})
)

type Errorpage struct {
	locales         *locales.ErrorpageLocales
	LocaleGenerator locales.ErrorpageLocalesGenerator
}

func (l *Errorpage) Render(ctx context.Context, w io.Writer) error {
	if l.LocaleGenerator == nil {
		return &TemplateRenderingError{uiComponentName: "errorpage", Err: &locales.LocalizationError{Message: "no locale generator provided"}}
	}
	if l.locales == nil {
		var err error
		if l.locales, err = l.LocaleGenerator.Generate(ctx); err != nil {
			return err
		}
	}
	if buf, err := renderTemplateToBuffer(errorPage, l.locales); err != nil {
		return err
	} else {
		buf.WriteTo(w)
		return nil
	}
}
