package ui

import (
	"context"
	"io"

	"gitlab.com/jgero/my-webpage/internal/locales"
)

var (
	blog = parseUnescaped("blog", []string{
		"templates/blog/index.html",
		"templates/blog/style.html",
	})
)

type Blogpage struct {
	locales         *locales.BlogpageLocales
	LocaleGenerator locales.BlogpageLocalesGenerator
}

func (b *Blogpage) Render(ctx context.Context, w io.Writer) error {
	if b.LocaleGenerator == nil {
		return &TemplateRenderingError{uiComponentName: "blogpage", Err: &locales.LocalizationError{Message: "no locale generator provided"}}
	}
	if b.locales == nil {
		var err error
		if b.locales, err = b.LocaleGenerator.Generate(ctx); err != nil {
			return err
		}
	}
	if buf, err := renderTemplateToBufferUnescaped(blog, b.locales); err != nil {
		return err
	} else {
		buf.WriteTo(w)
		return nil
	}
}
