package ui

import (
	"context"
	"io"

	"gitlab.com/jgero/my-webpage/internal/locales"
)

var (
	overview = parse("blog", []string{
		"templates/overview/index.html",
		"templates/overview/style.html",
	})
)

type Overviewpage struct {
	locales         *locales.OverviewLocales
	LocaleGenerator locales.OverviewLocalesGenerator
}

func (o *Overviewpage) Render(ctx context.Context, w io.Writer) error {
	if o.LocaleGenerator == nil {
		return &TemplateRenderingError{uiComponentName: "overview", Err: &locales.LocalizationError{Message: "no locale generator provided"}}
	}
	if o.locales == nil {
		var err error
		if o.locales, err = o.LocaleGenerator.Generate(ctx); err != nil {
			return err
		}
	}
	if buf, err := renderTemplateToBuffer(overview, o.locales); err != nil {
		return err
	} else {
		buf.WriteTo(w)
		return nil
	}
}
