package ui_test

import (
	"bytes"
	"context"
	"fmt"
	"testing"

	"gitlab.com/jgero/my-webpage/internal/constants"
	"gitlab.com/jgero/my-webpage/internal/locales"
	"gitlab.com/jgero/my-webpage/internal/webserver/ui"
)

func TestRenderErrorpage(t *testing.T) {
	tests := []struct {
		title            string
		errorCode        int
		errorExplanation string
		action           string
		lang             locales.Language
	}{
		{
			title:            "this is my page title",
			errorCode:        500,
			errorExplanation: "this was a bad internal thing",
			action:           "relay and pray the dev fixes it",
			lang:             locales.EN,
		},
		{
			title:            "das ist der titel meiner seite",
			errorCode:        404,
			errorExplanation: "eine seite wurde nicht gefunden",
			action:           "der link ist kaputt, versuche etwas anderes",
			lang:             locales.DE,
		},
	}

	for index, tc := range tests {
		tc := tc
		var buf bytes.Buffer
		t.Run(fmt.Sprintf("errorpage testcase %d", index), func(t *testing.T) {
			t.Parallel()

			ctx := context.WithValue(context.Background(), constants.LanguageCodeContextKey, tc.lang)
			ctx = context.WithValue(ctx, locales.ErrorCodeKey, tc.errorCode)

			uiEl := ui.Errorpage{
				LocaleGenerator: &locales.MockErrorpageLoader{
					LayoutGen:        &locales.StaticLayoutLoader{},
					Title:            tc.title,
					ErrorCode:        tc.errorCode,
					ErrorExplanation: tc.errorExplanation,
					Action:           tc.action,
				},
			}

			if err := uiEl.Render(ctx, &buf); err != nil {
				t.Log("should not return an error when rendering the errorpage with valid data")
				t.Logf("%s", err.Error())
				t.FailNow()
			}
			if &buf == nil {
				t.Log("rendering the template should not result in an 'nil' buffer")
				t.FailNow()
			}

			tokenList := []string{
				tc.title,
				fmt.Sprintf("%d", tc.errorCode),
				tc.errorExplanation,
				tc.action,
			}
			hasToken, err := checkHtmlDocForTextNodes(&buf, tokenList)
			if err != nil {
				t.Log("rendered document should be parseable and not encounter error tokens")
				t.Logf("%s", err.Error())
				t.FailNow()
			}
			for i, v := range hasToken {
				if !v {
					t.Errorf("token '%s' was not found in the document", tokenList[i])
				}
			}
		})
	}
}
