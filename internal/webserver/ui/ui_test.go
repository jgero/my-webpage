package ui_test

import (
	"bytes"
	"errors"
	"io"
	"strings"

	"golang.org/x/net/html"
)

func checkHtmlDocForTextNodes(buf *bytes.Buffer, textNodes []string) ([]bool, error) {
	// bool array will be initialized with the zero value of boolean which is 'false'
	hasTokenList := make([]bool, len(textNodes))
	tokenizer := html.NewTokenizer(buf)
	var lastStartToken html.Token

tokenLoop:
	for {
		tt := tokenizer.Next()
		switch {
		case tt == html.ErrorToken:
			if tokenizer.Err() == io.EOF {
				break tokenLoop
			}
			return nil, errors.New("found error token in html document")
		case tt == html.StartTagToken:
			lastStartToken = tokenizer.Token()
			continue
		case tt == html.TextToken:
			// don't check text content in js and css
			if lastStartToken.Data == "script" || lastStartToken.Data == "style" {
				continue
			}
			textContent := string(tokenizer.Text())
			for index, val := range textNodes {
				if strings.Contains(textContent, val) {
					hasTokenList[index] = true
				}
			}
		}

	}
	return hasTokenList, nil
}
