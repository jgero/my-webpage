package ui

import (
	"bytes"
	"context"
	"embed"
	"fmt"
	htmlTemplate "html/template"
	"io"
	textTemplate "text/template"
)

//go:embed templates/*.html templates/**/*.html
var htmlTemplates embed.FS

func parse(name string, files []string) *htmlTemplate.Template {
	// add the layout html and parse them all into one template
	return htmlTemplate.Must(htmlTemplate.ParseFS(htmlTemplates, append([]string{"templates/layout.html"}, files...)...))
}

func parseUnescaped(name string, files []string) *textTemplate.Template {
	// add the layout html and parse them all into one template
	return textTemplate.Must(textTemplate.ParseFS(htmlTemplates, append([]string{"templates/layout.html"}, files...)...))
}

type UiPage interface {
	Render(context.Context, io.Writer) error
}

// by writing to a buffer first it is ensured that errors while rendering
// templates does not result in partly written html responses
func renderTemplateToBuffer(template *htmlTemplate.Template, data interface{}) (*bytes.Buffer, error) {
	buf := &bytes.Buffer{}
	if err := template.Execute(buf, data); err != nil {
		return nil, &TemplateRenderingError{template.Name(), err}
	} else {
		return buf, nil
	}
}

// by writing to a buffer first it is ensured that errors while rendering
// templates does not result in partly written html responses
func renderTemplateToBufferUnescaped(template *textTemplate.Template, data interface{}) (*bytes.Buffer, error) {
	buf := &bytes.Buffer{}
	if err := template.Execute(buf, data); err != nil {
		return nil, &TemplateRenderingError{template.Name(), err}
	} else {
		return buf, nil
	}
}

type TemplateRenderingError struct {
	uiComponentName string
	Err             error
}

func (e *TemplateRenderingError) Error() string {
	return fmt.Sprintf("error occurred in component '%s': %s", e.uiComponentName, e.Err.Error())
}

func (e *TemplateRenderingError) Unwrap() error {
	return e.Err
}
