package webserver

import (
	"context"
	"fmt"
	"net/http"
	"regexp"
	"strconv"
	"strings"

	"gitlab.com/jgero/my-webpage/internal/constants"
	"gitlab.com/jgero/my-webpage/internal/locales"
	"gitlab.com/jgero/my-webpage/internal/store"
	"gitlab.com/jgero/my-webpage/internal/webserver/ui"
)

type UiRouteHandler struct {
	LandingpageGen  locales.LandingpageLocalesGenerator
	ErrorpageGen    locales.ErrorpageLocalesGenerator
	BlogpageGen     locales.BlogpageLocalesGenerator
	OverviewpageGen locales.OverviewLocalesGenerator
}

type ImageRouteHander struct {
	Conn store.ObjectGetterWriter
}

func (h *ImageRouteHander) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	trimmedRoute := strings.TrimPrefix(r.URL.Path, "/resources/images/")
	trimmedRoute = strings.ReplaceAll(trimmedRoute, " ", "_")
	blob := store.DataBlob{
		ResidingBucket: &store.Bucket{
			Name:   "images",
			Prefix: strings.Split(trimmedRoute, "/")[0],
		},
		Key: strings.Split(trimmedRoute, "/")[1],
	}
	if err := h.Conn.GetObject(r.Context(), &blob); err != nil {
		http.Redirect(w, r, "/en/error/500", http.StatusFound)
		return
	}

	w.Write(blob.Data)
}

func (h *UiRouteHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	// assert context value to langauge type
	lang, ok := r.Context().Value(constants.LanguageCodeContextKey).(locales.Language)
	if !ok {
		// report internal server error when langauge could not be found. redirecting to
		// the english startpage would not make sense because this would have already happened if
		// it was possible via the laocalize middleware
		http.Redirect(w, r, "/en/error/500", http.StatusFound)
		return
	}

	// handle route according to language type
	var delocalizedRoute = strings.TrimPrefix(r.URL.Path, "/"+lang.String())
	var uiPage ui.UiPage
	if regexp.MustCompile("^/?$").MatchString(delocalizedRoute) {
		// landingpage
		uiPage = &ui.Landingpage{LocaleGenerator: h.LandingpageGen}
	} else if regexp.MustCompile("^/blog/?$").MatchString(delocalizedRoute) {
		// add search string to context
		ctx = context.WithValue(r.Context(), constants.SearchStringKey, r.URL.Query().Get("searchString"))
		// blog overview
		uiPage = &ui.Overviewpage{
			LocaleGenerator: h.OverviewpageGen,
		}
	} else if regexp.MustCompile("^/blog/[a-zA-Z0-9 _-]+/?$").MatchString(delocalizedRoute) {
		// specific blog post
		// remove blog part of the route
		id := strings.TrimPrefix(delocalizedRoute, "/blog/")
		// remove a possible trailing "/"
		id = strings.TrimRight(id, "/")
		ctx = context.WithValue(r.Context(), constants.BlogpostKey, id)
		uiPage = &ui.Blogpage{LocaleGenerator: h.BlogpageGen}
	} else if regexp.MustCompile("^/error/[0-9]{3}/?$").MatchString(delocalizedRoute) {
		// errorpage
		// remove error part of the route
		c := strings.TrimPrefix(delocalizedRoute, "/error/")
		// remove a possible trailing "/"
		c = strings.TrimRight(c, "/")
		if errorCode, err := strconv.Atoi(c); err != nil {
			http.Error(w, "could not even render errorpage", http.StatusInternalServerError)
			return
		} else {
			ctx = context.WithValue(r.Context(), locales.ErrorCodeKey, errorCode)
			uiPage = &ui.Errorpage{LocaleGenerator: h.ErrorpageGen}
		}
	} else {
		// no regexp was matching that route, this means it probably is garbage
		http.Redirect(w, r, "/"+lang.String()+"/error/404", http.StatusFound)
		return
	}
	if err := uiPage.Render(ctx, w); err != nil {
		if _, ok := err.(*store.ObjectStoreObjectMissingError); ok {
			// check if non-existing blogpost was requested (no object in store for that route)
			fmt.Println(err.Error())
			http.Redirect(w, r, "/"+lang.String()+"/error/404", http.StatusFound)
		} else {
			// otherwise it has to be an actual internal server error
			fmt.Println(err.Error())
			http.Redirect(w, r, "/"+lang.String()+"/error/500", http.StatusFound)
		}
		return
	}
}
