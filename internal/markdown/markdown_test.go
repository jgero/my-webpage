package markdown_test

import (
	"strings"
	"testing"

	"gitlab.com/jgero/my-webpage/internal/markdown"
)

func TestLinkRendering(t *testing.T) {
	title := "this link is a example"
	link := "https://example.com"
	md := "[" + title + "](" + link + ")"
	html, _ := markdown.MarkdownToHtml(md, "")
	if !strings.Contains(html, title) {
		t.Errorf("link should contain the provided markdown title")
	}
	if !strings.Contains(html, link) {
		t.Errorf("link should contain the provided url")
	}
	if !strings.Contains(html, "target=\"_blank\"") {
		t.Errorf("link should have target _blank to open it in a new tab")
	}
}
func TestExternalImageRendering(t *testing.T) {
	title := "this link is a example"
	link := "https://example.com"
	md := "![" + title + "](" + link + ")"
	html, _ := markdown.MarkdownToHtml(md, "/my/prefix/")
	if !strings.Contains(html, title) {
		t.Errorf("image should contain the provided markdown title")
	}
	expectedUrl := `src="` + link + `"`
	if !strings.Contains(html, expectedUrl) {
		t.Logf("expected '%s' to contain '%s'", html, expectedUrl)
		t.Errorf("image source should be unmodified url")
	}
	expectedAlt := `alt="` + title + `"`
	if !strings.Contains(html, expectedAlt) {
		t.Logf("expected '%s' to contain '%s'", html, expectedAlt)
		t.Errorf("image should contain correct alt tag")
	}
}
func TestInternalImageRendering(t *testing.T) {
	title := "this link is a example"
	link := "my_nice_image.jpg"
	prefix := "/my/prefix/"
	md := "![" + title + "](" + link + ")"
	html, _ := markdown.MarkdownToHtml(md, prefix)
	if !strings.Contains(html, title) {
		t.Errorf("image should contain the provided markdown title")
	}
	expectedUrl := `src="` + prefix + link + `"`
	if !strings.Contains(html, expectedUrl) {
		t.Logf("expected '%s' to contain '%s'", html, expectedUrl)
		t.Errorf("image source should be provided filename prefixed with the prefix")
	}
	expectedAlt := `alt="` + title + `"`
	if !strings.Contains(html, expectedAlt) {
		t.Logf("expected '%s' to contain '%s'", html, expectedAlt)
		t.Errorf("image should contain correct alt tag")
	}
}
