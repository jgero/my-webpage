package markdown

import (
	"bytes"
	"fmt"
	"html"
	"io"
	"strings"
	"text/template"

	"github.com/gomarkdown/markdown"
	"github.com/gomarkdown/markdown/ast"
	mdHtml "github.com/gomarkdown/markdown/html"
	"github.com/gomarkdown/markdown/parser"
)

type tocEntry struct {
	Level   int
	Content string
	Id      string
}

var (
	toc                   []tocEntry = []tocEntry{}
	description           string
	hasReadFirstParagraph bool = false
	imageLinkPrefix       string
)

func customRenderer(w io.Writer, node ast.Node, entering bool) (ast.WalkStatus, bool) {
	if linkNode, ok := node.(*ast.Link); entering && ok && linkNode.NoteID == 0 {
		// nodes that are a link and are not a footnote link
		if strings.HasPrefix(string(linkNode.Destination), "http") {
			// check for http prefix to only act on links to external sites
			out := "<a href=\""
			out += string(linkNode.Destination)
			out += "\" referrerpolicy=\"no_referrer\" target=\"_blank\""
			if len(linkNode.Title) > 0 {
				out += " title=\""
				out += string(linkNode.Title)
				out += "\""
			}
			// other attrs are ignored in this case
			out += ">"
			io.WriteString(w, out)
			// return true means skip -> node is already rendered now
			return ast.GoToNext, true
		}
	} else if headingNode, ok := node.(*ast.Heading); entering && ok {
		// track headings for the table of contents
		toc = append(toc, tocEntry{
			Level: headingNode.Level,
			// headings always have exatly one leaf child so this is safe
			Content: string(headingNode.GetChildren()[0].AsLeaf().Literal),
			Id:      headingNode.HeadingID,
		})
		// return false means node still has to be rendered
		return ast.GoToNext, false
	} else if paragraphNode, ok := node.(*ast.Paragraph); entering && ok && !hasReadFirstParagraph {
		description = string(paragraphNode.GetChildren()[0].AsLeaf().Literal)
		hasReadFirstParagraph = true
		// return false means node still has to be rendered
		return ast.GoToNext, false
	} else if imageNode, ok := node.(*ast.Image); entering && ok {
		// leave images to external sources as they are
		if strings.HasPrefix(string(imageNode.Destination), "http") {
			return ast.GoToNext, false
		}

		out := "<img src=\""
		out += imageLinkPrefix
		out += string(imageNode.Destination)
		out += "\" alt=\""
		// out += string(imageNode.Title)
		// out += "\" >"
		io.WriteString(w, out)
		// return true means skip -> node is already rendered now
		return ast.GoToNext, true
	} else if imageNode, ok := node.(*ast.Image); !entering && ok {
		// leave images to external sources as they are
		if strings.HasPrefix(string(imageNode.Destination), "http") {
			return ast.GoToNext, false
		}

		out := "\" >"
		io.WriteString(w, out)
		// return true means skip -> node is already rendered now
		return ast.GoToNext, true
	}
	// return false means node still has to be rendered
	return ast.GoToNext, false
}

// takes some markdown text and a prefix to put in front of internal image links
// returns the html string for the content and for the description of the article
func MarkdownToHtml(input string, imgLinkPrefix string) (string, string) {
	// reset global vars
	toc = make([]tocEntry, 0)
	hasReadFirstParagraph = false
	description = ""
	imageLinkPrefix = imgLinkPrefix

	extensions := parser.CommonExtensions | parser.FencedCode | parser.Footnotes | parser.AutoHeadingIDs
	p := parser.NewWithExtensions(extensions)
	opts := mdHtml.RendererOptions{
		Flags:          mdHtml.CommonFlags,
		RenderNodeHook: customRenderer,
	}
	renderer := mdHtml.NewRenderer(opts)
	content := string(markdown.ToHTML([]byte(input), p, renderer))
	tocString, err := renderToc(toc)
	if err != nil {
		// TODO: log error instead of useless printing
		fmt.Println(err)
	}
	content = tocString + content
	return content, html.EscapeString(description)
}

func renderToc(toc []tocEntry) (string, error) {
	const tocTemplate = `<ul class="table-of-contents">{{range .}}<li><a href="#{{.Id}}" class="level-{{.Level}}">{{.Content}}</a></li>{{end}}</ul>`
	templ, err := template.New("toc").Parse(tocTemplate)
	if err != nil {
		return "", err
	}
	buf := &bytes.Buffer{}
	err = templ.Execute(buf, toc)
	if err != nil {
		return "", err
	}
	return buf.String(), nil
}
