package store

import (
	"context"
	"strings"
	"time"
)

type ContentType int

const (
	TEXT ContentType = iota
	JSON
	OCTET
)

type CommandType int

const (
	WRITE CommandType = iota
	CREATE
	DELETE
)

func (c ContentType) String() string {
	switch c {
	case TEXT:
		return "text/plain"
	case JSON:
		return "application/json"
	case OCTET:
		return "application/octet-stream"
	}
	return "application/octet-stream"
}

func ToContentType(ts string) ContentType {
	switch ts {
	case "text/plain":
		return TEXT
	case "application/json":
		return JSON
	case "application/octet-stream":
		return OCTET
	}
	return OCTET
}

func KeyToTitle(key string) string {
	title := strings.TrimSuffix(key, ".md")
	title = strings.ReplaceAll(title, "_", " ")
	return title
}

func TitleToKey(title string) string {
	key := strings.ReplaceAll(title, " ", "_")
	key = key + ".md"
	return key
}

func TitleToPrefix(title string) string {
	key := "/resources/images/" + strings.ReplaceAll(title, " ", "_") + "/"
	return key
}

type Bucket struct {
	// the name of the bucket
	Name string
	// the prefix of the object(s) in the bucket
	// should not have leading and trailing "/"
	Prefix string
}

type DataBlob struct {
	ResidingBucket *Bucket
	Key            string
	Type           ContentType
	Data           []byte
}

type BlobsInfo struct {
	ResidingBucket *Bucket
	Info           []struct {
		Key          string
		LastModified time.Time
	}
}

type ObjectGetterWriter interface {
	GetObject(context.Context, *DataBlob) error
	GetAllObjectInfos(context.Context, *BlobsInfo) error
	WriteObject(context.Context, *DataBlob, CommandType) error
}
