package store

import (
	"context"
	"errors"
	"time"
)

type DefaultConnectionSpy struct {
	UpstreamData                   string
	UpstreamType                   ContentType
	GetObjectCalledCounter         int
	GetAllObjectInfosCalledCounter int
	WriteObjectCalledCounter       int
}

func (conn *DefaultConnectionSpy) GetObject(ctx context.Context, blob *DataBlob) error {
	conn.GetObjectCalledCounter++
	blob.Data = []byte(conn.UpstreamData)
	blob.Type = conn.UpstreamType
	return nil
}

func (conn *DefaultConnectionSpy) GetAllObjectInfos(ctx context.Context, info *BlobsInfo) error {
	conn.GetAllObjectInfosCalledCounter++
	infos := make([]struct {
		Key          string
		LastModified time.Time
	}, 2)
	infos[0] = struct {
		Key          string
		LastModified time.Time
	}{Key: "my_test_key.md", LastModified: time.Now()}
	infos[1] = struct {
		Key          string
		LastModified time.Time
	}{Key: "my_second_test_key.md", LastModified: time.Now()}
	info.Info = infos
	return nil
}

func (conn *DefaultConnectionSpy) WriteObject(ctx context.Context, blob *DataBlob, t CommandType) error {
	conn.WriteObjectCalledCounter++
	conn.UpstreamData = string(blob.Data)
	conn.UpstreamType = blob.Type
	return nil
}

type FailingConnectionSpy struct {
	UpstreamData                   string
	UpstreamType                   ContentType
	GetObjectCalledCounter         int
	GetAllObjectInfosCalledCounter int
	WriteObjectCalledCounter       int
}

func (conn *FailingConnectionSpy) GetObject(ctx context.Context, blob *DataBlob) error {
	conn.GetObjectCalledCounter++
	return &ObjectStoreObjectMissingError{ObjectKey: "test-any-key.md", Err: errors.New("always return missing")}
}

func (conn *FailingConnectionSpy) WriteObject(ctx context.Context, blob *DataBlob, t CommandType) error {
	conn.WriteObjectCalledCounter++
	conn.UpstreamData = string(blob.Data)
	conn.UpstreamType = blob.Type
	return errors.New("write object failed")
}

func (conn *FailingConnectionSpy) GetAllObjectInfos(ctx context.Context, info *BlobsInfo) error {
	conn.GetAllObjectInfosCalledCounter++
	return errors.New("get all object infos failed")
}
