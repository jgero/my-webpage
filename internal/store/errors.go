package store

import "fmt"

type ObjectStoreConnectionError struct {
	Message string
	Err     error
}

func (err ObjectStoreConnectionError) Error() string {
	return fmt.Sprintf("could not connect to object store: %s: %s", err.Message, err.Err.Error())
}

func (err ObjectStoreConnectionError) Unwrap() error {
	return err.Err
}

type ObjectStoreQueryError struct {
	Message string
	Err     error
}

func (err ObjectStoreQueryError) Error() string {
	return fmt.Sprintf("query failed: %s: %s", err.Message, err.Err.Error())
}

func (err ObjectStoreQueryError) Unwrap() error {
	return err.Err
}

type ObjectStoreObjectMissingError struct {
	ObjectKey string
	Err       error
}

func (err ObjectStoreObjectMissingError) Error() string {
	return fmt.Sprintf("object missing: %s: %s", err.ObjectKey, err.Err.Error())
}

func (err ObjectStoreObjectMissingError) Unwrap() error {
	return err.Err
}

type ObjectStoreCommandError struct {
	Message string
	Err     error
}

func (err ObjectStoreCommandError) Error() string {
	return fmt.Sprintf("command failed: %s: %s", err.Message, err.Err.Error())
}

func (err ObjectStoreCommandError) Unwrap() error {
	return err.Err
}
