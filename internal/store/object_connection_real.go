package store

import (
	"bytes"
	"context"
	"io/ioutil"
	"os"
	"strings"
	"time"

	"github.com/minio/minio-go/v7"
	"github.com/minio/minio-go/v7/pkg/credentials"
)

type objectstoreConnection struct {
	client *minio.Client
}

func (conn *objectstoreConnection) GetObject(ctx context.Context, blob *DataBlob) error {
	object, err := conn.client.GetObject(ctx, blob.ResidingBucket.Name, blob.ResidingBucket.Prefix+"/"+blob.Key, minio.GetObjectOptions{})
	if err != nil {
		return &ObjectStoreQueryError{Message: "could not get object from store", Err: err}
	}

	blob.Data, err = ioutil.ReadAll(object)
	if err != nil {
		object.Close()
		return &ObjectStoreObjectMissingError{ObjectKey: blob.Key, Err: err}
	}
	meta, err := object.Stat()
	if err != nil {
		return &ObjectStoreQueryError{Message: "could not stat object", Err: err}
	}
	err = object.Close()
	if err != nil {
		return &ObjectStoreQueryError{Message: "could not close object", Err: err}
	}
	blob.Type = ToContentType(meta.ContentType)
	return nil
}

func (conn *objectstoreConnection) GetAllObjectInfos(ctx context.Context, info *BlobsInfo) error {
	objectCh := conn.client.ListObjects(ctx, info.ResidingBucket.Name, minio.ListObjectsOptions{Prefix: info.ResidingBucket.Prefix, Recursive: true})
	info.Info = make([]struct {
		Key          string
		LastModified time.Time
	}, 0)
	for object := range objectCh {
		if object.Err != nil {
			return &ObjectStoreQueryError{Message: "could not get info of all objects in bucket", Err: object.Err}
		}
		info.Info = append(info.Info, struct {
			Key          string
			LastModified time.Time
		}{Key: strings.TrimPrefix(object.Key, info.ResidingBucket.Prefix+"/"), LastModified: object.LastModified})
	}
	return nil
}

func (conn *objectstoreConnection) WriteObject(ctx context.Context, blob *DataBlob, t CommandType) error {
	switch t {
	case WRITE:
	case CREATE:
		_, err := conn.client.PutObject(
			ctx,
			blob.ResidingBucket.Name,
			blob.Key,
			bytes.NewReader(blob.Data),
			int64(len(blob.Data)),
			minio.PutObjectOptions{ContentType: blob.Type.String()},
		)
		if err != nil {
			return &ObjectStoreCommandError{Message: "could not write object", Err: err}
		}
		return nil
	case DELETE:
		err := conn.client.RemoveObject(ctx, blob.ResidingBucket.Name, blob.Key, minio.RemoveObjectOptions{})
		if err != nil {
			return &ObjectStoreCommandError{Message: "could not delete object", Err: err}
		}
		return nil
	}
	return nil
}

func GetObjectstoreConnection() (ObjectGetterWriter, error) {
	url := os.Getenv("OBJECT_STORE_URL")
	port := os.Getenv("OBJECT_STORE_PORT")
	var endpoint string
	useSSL := true
	if port == "" {
		endpoint = url
	} else {
		endpoint = url + ":" + port
		useSSL = false
	}
	accessKeyID := os.Getenv("OBJECT_STORE_ACCESS_KEY")
	secretAccessKey := os.Getenv("OBJECT_STORE_SECRET_KEY")

	storeClient, err := minio.New(endpoint, &minio.Options{
		Creds:  credentials.NewStaticV4(accessKeyID, secretAccessKey, ""),
		Secure: useSSL,
	})
	if err != nil {
		return nil, ObjectStoreConnectionError{Message: "could not connect to object store", Err: err}
	}
	return &objectstoreConnection{client: storeClient}, nil
}
